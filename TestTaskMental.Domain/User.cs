﻿using System;
using System.Collections.Generic;
using System.Text;
using TestTaskMental.Domain.Interfaces;

namespace TestTaskMental.Domain
{
    public class User: IRemovableEntity
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public DateTime RegistrationDate { get; set; }
        public bool IsRemoved { get; set; }
    }
}
