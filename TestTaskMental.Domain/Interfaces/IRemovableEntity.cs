﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestTaskMental.Domain.Interfaces
{
    public interface IRemovableEntity
    {
        bool IsRemoved { get; set; }
    }
}
