﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestTaskMental.Domain.Interfaces
{
    public interface IAuditableEditorEntity
    {
        string CreatedBy { get; set; }
        DateTime CreatedOn { get; set; }
        string EditedBy { get; set; }
        DateTime EditedOn { get; set; }
    }
}
