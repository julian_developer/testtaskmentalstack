﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestTaskMental.Domain
{
    public class ScheduleType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
