﻿using System;
using TestTaskMental.Domain.Interfaces;

namespace TestTaskMental.Domain
{
    public class Schedule: IAuditableEditorEntity, IRemovableEntity
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Type { get; set; }
        public DateTime ScheduleDateTime { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime EditedOn { get; set; }
        public DateTime NotificationDateTime { get; set; }
        public Guid UserId { get; set; }
        public bool IsRemoved { get; set; }
        public string CreatedBy { get; set; }
        public string EditedBy { get; set; }
    }
}
