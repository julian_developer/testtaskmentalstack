﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using TestTaskMental.BL.Mapper;
using TestTaskMental.BL.Models;
using TestTaskMental.Common;
using TestTaskMental.DAL;
using TestTaskMental.Domain;
using Microsoft.Extensions.Logging;

namespace TestTaskMental.BL.Engine
{
    public class ScheduleEngine : IScheduleEngine
    {
        private readonly ILogger<ScheduleEngine> _logger;
        private readonly IContext _context;
        private readonly IScheduleMapper _mapper;
        private const string ErrorText = " Попробуйте снова или обратитесь в службу технической поддержки.";

        public ScheduleEngine(ILogger<ScheduleEngine> logger, Context context, IScheduleMapper mapper)
        {
            _logger = logger;
            _context = context;
            _mapper = mapper;
        }

        public MethodResult<ScheduleDto> AddSchedule(ScheduleDto schedule, string userLogin)
        {
            try
            {
                var user = _context.Users
                    .AsNoTracking()
                    .Where(x => !x.IsRemoved)
                    .FirstOrDefault(x => x.Login == userLogin);

                if (user == null)
                {
                    return $"Пользователя с таким логином не сущестует. {ErrorText}".ToErrorMethodResult<ScheduleDto>();
                }

                var mappedResult = _mapper.TryMapViewModelToEntity<ScheduleDto, Schedule>(schedule);
                if (!mappedResult.IsOk)
                {
                    return mappedResult.To<Schedule, ScheduleDto>();
                }

                var scheduleEntity = mappedResult.Value;

                scheduleEntity.UserId = user.Id;
                _context.AddAuditableEntity(scheduleEntity, userLogin);
                _context.SaveChanges();

                var addedSceduleMappedResult = _mapper.TryMapEntityToViewModel<ScheduleDto, Schedule>(scheduleEntity);
                if (!addedSceduleMappedResult.IsOk)
                {
                    return addedSceduleMappedResult;
                }

                return addedSceduleMappedResult.Value.ToSuccessMethodResult();

            }
            catch (Exception e)
            {
                var message = $"Не удалось добавить запись. {ErrorText}";
                _logger.LogError(e, message);
                return message.ToErrorMethodResult<ScheduleDto>();
            }
        }

        public MethodResult<List<ScheduleDto>> GetAllSchedules(string userLogin)
        {
            try
            {
                var user = _context.Users
                    .AsNoTracking()
                    .Where(x => !x.IsRemoved)
                    .FirstOrDefault(x => x.Login == userLogin);

                if (user == null)
                {
                    return $"Пользователя с таким логином не сущестует. {ErrorText}".ToErrorMethodResult<List<ScheduleDto>>();
                }

                var schedules = _context.Schedules
                    .AsNoTracking()
                    .Where(x => !x.IsRemoved)
                    .Where(x => x.UserId == user.Id)
                    .ToList()
                    .Select(x => _mapper.TryMapEntityToViewModel<ScheduleDto, Schedule>(x))
                    .Where(x => x.IsOk)
                    .Select(x => x.Value)
                    .OrderBy(c => c.ScheduleDateTime)
                    .ToList();

                return schedules.ToSuccessMethodResult();
            }
            catch (Exception e)
            {
                var message = $"Не удалось добавить запись. {ErrorText}";
                _logger.LogError(e, message);
                return message.ToErrorMethodResult<List<ScheduleDto>>();
            }
        }

        public MethodResult<List<ScheduleTypeDto>> GetAllScheduleTypes()
        {
            try
            {
                var scheduleTypes = _context.ScheduleTypes
                    .AsNoTracking()
                    .ToList()
                    .Select(x => _mapper.TryMapEntityToViewModel<ScheduleTypeDto, ScheduleType>(x))
                    .Where(x => x.IsOk)
                    .Select(x => x.Value)
                    .ToList();

                return scheduleTypes.ToSuccessMethodResult();
            }
            catch (Exception e)
            {
                var message = $"Не удалось получить список типов задачи. {ErrorText}";
                _logger.LogError(e, message);
                return message.ToErrorMethodResult<List<ScheduleTypeDto>>();
            }
        }

        public MethodResult<ScheduleDto> RemoveSchedule(long scheduleId, string userLogin)
        {
            try
            {
                var scheduleEntity = _context.Schedules
                    .Where(x => !x.IsRemoved)
                    .FirstOrDefault(x => x.Id == scheduleId);

                if (scheduleEntity == null)
                {
                    return $"Выбранной задачи не существует. {ErrorText}".ToErrorMethodResult<ScheduleDto>();
                }

                _context.RemoveAuditableEntity(scheduleEntity, userLogin);
                _context.SaveChanges();

                var mappedResult = _mapper.TryMapEntityToViewModel<ScheduleDto, Schedule>(scheduleEntity);
                if (!mappedResult.IsOk)
                {
                    return mappedResult;
                }

               return mappedResult.Value.ToSuccessMethodResult();
            }
            catch (Exception e)
            {
                var message = $"Не удалось удалить запись. {ErrorText}";
                _logger.LogError(e, message);
                return message.ToErrorMethodResult<ScheduleDto>();
            }
        }

        public MethodResult<ScheduleDto> UpdateSchedule(ScheduleDto schedule, string userLogin)
        {
            try
            {
                var user = _context.Users
                    .AsNoTracking()
                    .Where(x => !x.IsRemoved)
                    .FirstOrDefault(x => x.Login == userLogin);

                if (user == null)
                {
                    return $"Пользователя с таким логином не сущестует. {ErrorText}".ToErrorMethodResult<ScheduleDto>();
                }

                var mappedResult = _mapper.TryMapViewModelToEntity<ScheduleDto, Schedule>(schedule);
                if (!mappedResult.IsOk)
                {
                    return mappedResult.To<Schedule, ScheduleDto>();
                }

                var scheduleEntity = mappedResult.Value;

                scheduleEntity.UserId = user.Id;
                _context.UpdateAuditableEntity(scheduleEntity, userLogin);
                _context.SaveChanges();

                return schedule.ToSuccessMethodResult();

            }
            catch (Exception e)
            {
                var message = $"Не удалось обновить запись. {ErrorText}";
                _logger.LogError(e, message);
                return message.ToErrorMethodResult<ScheduleDto>();
            }
        }

        public MethodResult<List<ScheduleDto>> GetAllSchedulesForNotification(string userLogin, string connectionString)
        {
            try
            {
                var result = new List<ScheduleDto>();
                using (var context = new Context(connectionString))
                {
                    var dateNow = DateTime.Now;
                    result = context.Schedules
                        .AsNoTracking()
                        .Where(x => !x.IsRemoved)
                        .Where(x => x.NotificationDateTime.Date == dateNow.Date)
                        .Where(x => x.NotificationDateTime.Hour == dateNow.Hour)
                        .Where(x => x.NotificationDateTime.Minute == dateNow.Minute)
                        .ToList()
                        .Select(x => _mapper.TryMapEntityToViewModel<ScheduleDto, Schedule>(x))
                        .Where(x => x.IsOk)
                        .Select(x => x.Value)
                        .ToList();
                }

                return result.ToSuccessMethodResult();
            }
            catch (Exception e)
            {
                var message = $"Не удалось получить данные для нотификации.";
                _logger.LogError(e, message);
                return message.ToErrorMethodResult<List<ScheduleDto>>();
            }
        }
    }
}
