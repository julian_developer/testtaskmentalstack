﻿using System;
using System.Collections.Generic;
using System.Text;
using TestTaskMental.BL.Models;
using TestTaskMental.Common;

namespace TestTaskMental.BL.Engine
{
    public interface IScheduleEngine
    {
        MethodResult<ScheduleDto> AddSchedule(ScheduleDto schedule, string userLogin);
        MethodResult<ScheduleDto> RemoveSchedule(long scheduleId, string userLogin);
        MethodResult<ScheduleDto> UpdateSchedule(ScheduleDto schedule, string userLogin);
        MethodResult<List<ScheduleDto>> GetAllSchedules(string userLogin);
        MethodResult<List<ScheduleTypeDto>> GetAllScheduleTypes();
        MethodResult<List<ScheduleDto>> GetAllSchedulesForNotification(string userLogin, string connectionString);
    }
}
