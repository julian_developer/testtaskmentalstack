﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestTaskMental.BL.Models
{
    public class ScheduleTypeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
