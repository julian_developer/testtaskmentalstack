﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestTaskMental.BL.Models
{
    public class ScheduleDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Type { get; set; }
        public DateTime ScheduleDateTime { get; set; }
        public DateTime NotificationDateTime { get; set; }
    }
}
