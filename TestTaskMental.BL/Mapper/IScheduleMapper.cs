﻿using System;
using System.Collections.Generic;
using System.Text;
using TestTaskMental.Common;

namespace TestTaskMental.BL.Mapper
{

    public interface IScheduleMapper
    {
        /// <summary>
        /// ViewModel >>> Entity
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TTarget"></typeparam>
        /// <param name="sourceViewModel"></param>
        /// <param name="targetEntity"></param>
        /// <returns></returns>
        MethodResult<TTarget> TryMapViewModelToEntity<TSource, TTarget>(TSource sourceViewModel, TTarget targetEntity = default(TTarget)) where TTarget : class, new() where TSource : class;

        /// <summary>
        /// Entity >>> ViewModel
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TTarget"></typeparam>
        /// <param name="sourceEntity"></param>
        /// <param name="targetViewModel"></param>
        /// <returns></returns>
        MethodResult<TTarget> TryMapEntityToViewModel<TTarget, TSource>(TSource sourceEntity, TTarget targetViewModel = default(TTarget)) where TTarget : new();

        MethodResult<TTarget> TryUpdateViewModel<TTarget, TSource>(TSource sourceViewModel, TTarget targetViewModel);
    }
}
