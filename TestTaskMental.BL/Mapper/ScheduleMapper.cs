﻿using AutoMapper;
using AutoMapper.EquivalencyExpression;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using TestTaskMental.BL.Models;
using TestTaskMental.Common;
using TestTaskMental.Domain;

namespace TestTaskMental.BL.Mapper
{
    public class ScheduleMapper : IScheduleMapper
    {
        private readonly ILogger<ScheduleMapper> _logger;
        private static readonly IMapper _mapper;

        static ScheduleMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddCollectionMappers();

                
                cfg.CreateMap<Schedule, ScheduleDto>();
                cfg.CreateMap<ScheduleDto, Schedule>();
            });

            _mapper = config.CreateMapper();
        }

        /// <summary>Initializes a new instance of the <see cref="T:System.Object" /> class.</summary>
        public ScheduleMapper(ILogger<ScheduleMapper> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// ViewModel >>> Entity
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="sourceViewModel"></param>
        /// <param name="targetEntity"></param>
        /// <returns></returns>
        public MethodResult<T2> TryMapViewModelToEntity<T1, T2>(T1 sourceViewModel, T2 targetEntity = default(T2)) where T2 : class, new() where T1 : class
        {
            try
            {
                if (targetEntity == null)
                {
                    return _mapper.Map<T2>(sourceViewModel).ToSuccessMethodResult();
                }

                _mapper.Map(sourceViewModel, targetEntity);

                return targetEntity.ToSuccessMethodResult();
            }
            catch (Exception ex)
            {
                var message = "Не удалось сопоставить модель для отображения с моделью данных из БД";
                _logger.LogError(ex, message);
                return MethodResult<T2>.GetErrorResult(message, MethodResultLevel.BL);
            }
        }

        /// <summary>
        /// Entity >>> ViewModel
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="sourceEntity"></param>
        /// <param name="targetViewModel"></param>
        /// <returns></returns>
        public MethodResult<TTarget> TryMapEntityToViewModel<TTarget, TSource>(TSource sourceEntity, TTarget targetViewModel = default(TTarget)) where TTarget : new()
        {
            try
            {
                if (targetViewModel == null)
                {
                    targetViewModel = new TTarget();
                }
                _mapper.Map(sourceEntity, targetViewModel);

                return targetViewModel.ToSuccessMethodResult();
            }
            catch (Exception ex)
            {
                var message = "Не удалось сопоставить модель данных из БД с моделью для отображения";
                _logger.LogError(ex, message);
                return MethodResult<TTarget>.GetErrorResult(message, MethodResultLevel.BL);
            }
        }

        /// <summary>
        /// ViewModel >>> ViewModel
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="sourceViewModel"></param>
        /// <param name="targetViewModel"></param>
        /// <returns></returns>
        public MethodResult<TTarget> TryUpdateViewModel<TTarget, TSource>(TSource sourceViewModel, TTarget targetViewModel)
        {
            try
            {
                _mapper.Map(sourceViewModel, targetViewModel);

                return targetViewModel.ToSuccessMethodResult();
            }
            catch (Exception ex)
            {
                var message = "Не удалось сопоставить модель данных из БД с моделью для отображения";
                _logger.LogError(ex, message);
                return MethodResult<TTarget>.GetErrorResult(message, MethodResultLevel.BL);
            }
        }
    }
}
