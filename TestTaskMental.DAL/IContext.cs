﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using TestTaskMental.Domain;
using TestTaskMental.Domain.Interfaces;

namespace TestTaskMental.DAL
{
    public interface IContext
    {
        IEnumerable<T> SetEntity<T>() where T : class;

        T AddEntity<T>(T entity) where T : class;

        T RemoveEntity<T>(T entity) where T : class;

        void UpdateEntity<T>(T entity) where T : class;

        int SaveChanges();

        DbSet<Schedule> Schedules { get; }
        DbSet<User> Users { get; }
        DbSet<ScheduleType> ScheduleTypes { get; }

        T AddAuditableEntity<T>(T entity) where T : class, IAuditableEditorEntity;

        T AddAuditableEntity<T>(T entity, string userLogin) where T : class, IAuditableEditorEntity;

        void UpdateAuditableEntity<T>(T entity) where T : class, IAuditableEditorEntity;

        void UpdateAuditableEntity<T>(T entity, string userLogin) where T : class, IAuditableEditorEntity;

        void RemoveAuditableEntity<T>(T entity) where T : class, IAuditableEditorEntity, IRemovableEntity;

        void RemoveAuditableEntity<T>(T entity, string userLogin) where T : class, IAuditableEditorEntity, IRemovableEntity;
    }
}
