﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestTaskMental.Domain;
using TestTaskMental.Domain.Interfaces;

namespace TestTaskMental.DAL
{
    public class Context : DbContext, IContext, IDisposable
    {
        public Context(DbContextOptions<Context> options)
            : base(options)
        {
           // Database.EnsureCreated();
        }

        private readonly string _connectionString;

        public Context(string connectionString): base()
        {
            _connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!string.IsNullOrWhiteSpace(_connectionString))
                optionsBuilder.UseSqlServer(_connectionString);
        }

        public void UpdateEntity<T>(T entity) where T : class
        {
            Entry(entity).State = EntityState.Modified;
        }
        
        public IEnumerable<T> SetEntity<T>() where T : class
        {
            return Set<T>();
        }

        public T AddEntity<T>(T entity) where T : class
        {
            return Set<T>().Add(entity).Entity;
        }

        public T RemoveEntity<T>(T entity) where T : class
        {
            return Set<T>().Remove(entity).Entity;
        }

        public T AddAuditableEntity<T>(T entity) where T : class, IAuditableEditorEntity
        {
            entity.CreatedOn = DateTime.Now;
            entity.EditedOn = DateTime.Now;
            return AddEntity(entity);
        }

        public T AddAuditableEntity<T>(T entity, string userLogin) where T : class, IAuditableEditorEntity
        {
            entity.CreatedBy = userLogin;
            entity.EditedBy = userLogin;
            return AddAuditableEntity(entity);
        }

        public void UpdateAuditableEntity<T>(T entity) where T : class, IAuditableEditorEntity
        {
            UpdateEntity(entity);
            Entry(entity).Property(o => o.CreatedBy).IsModified = false;
            Entry(entity).Property(o => o.CreatedOn).IsModified = false;
        }

        public void UpdateAuditableEntity<T>(T entity, string userLogin) where T : class, IAuditableEditorEntity
        {
            entity.EditedBy = userLogin;
            entity.EditedOn = DateTime.Now;
            UpdateAuditableEntity(entity);
        }

        public void RemoveAuditableEntity<T>(T entity) where T : class, IAuditableEditorEntity, IRemovableEntity
        {
            entity.IsRemoved = true;
            UpdateAuditableEntity(entity);
        }

        public void RemoveAuditableEntity<T>(T entity, string userLogin) where T : class, IAuditableEditorEntity, IRemovableEntity
        {
            entity.IsRemoved = true;
            UpdateAuditableEntity(entity, userLogin);
        }

        DbSet<Schedule> IContext.Schedules => Schedules;
        DbSet<User> IContext.Users => Users;
        DbSet<ScheduleType> IContext.ScheduleTypes => ScheduleTypes;

        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<ScheduleType> ScheduleTypes { get; set; }
    }
}
