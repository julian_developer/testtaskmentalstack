﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestTaskMental.Common
{
    public class MethodResult<TValueClass>
    {
        public TValueClass Value { get; set; }

        public bool IsOk { get; set; }

        public string Message { get; set; }

        public string ExceptionMessage { get; set; }

        public List<string> ErrorList { get; set; }

        public MethodResultLevel ResultLevel { get; set; }

        private Exception _exception { get; set; }

        public MethodResult<TValueClass> SetException(Exception ex)
        {
            this._exception = ex;
            this.ExceptionMessage = this.GetFullExceptionMessage();
            return this;
        }

        public Exception GetException()
        {
            return this._exception;
        }

        private string GetFullExceptionMessage(Exception ex)
        {
            StringBuilder stringBuilder = new StringBuilder();
            if (ex == null)
            {
                return stringBuilder.ToString();
            }

            stringBuilder.AppendFormat("Message: {0}; Текст ошибки: {1}", (object)this.Message, (object)ex.Message);
            if (ex.InnerException != null)
            {
                stringBuilder.AppendFormat("\r\nВнутренняя ошибка: {0}", (object)this.GetFullExceptionMessage(ex.InnerException));
            }

            return stringBuilder.ToString();
        }

        public string GetFullExceptionMessage()
        {
            if (this._exception == null)
            {
                return string.Format("Message: {0}; Exception отсутствует", (object)this.Message);
            }

            return string.Format("Описание:{0}\r\nTrace:{1}", (object)this.GetFullExceptionMessage(this._exception), (object)this._exception.StackTrace);
        }

        public MethodResult()
        {
            this.IsOk = false;
            this.Message = "Не определено";
            this.ResultLevel = MethodResultLevel.Unknown;
        }

        public static MethodResult<TValueClass> GetErrorResult(string message = "", MethodResultLevel level = MethodResultLevel.Unknown, Exception ex = null)
        {
            MethodResult<TValueClass> methodResult = new MethodResult<TValueClass>();
            methodResult._exception = ex;
            methodResult.IsOk = false;
            methodResult.Message = message;
            methodResult.ResultLevel = level;
            methodResult.ExceptionMessage = methodResult.GetFullExceptionMessage();
            return methodResult;
        }

        public static MethodResult<TValueClass> GetExceptionResult(string message, MethodResultLevel level, Exception ex)
        {
            MethodResult<TValueClass> methodResult = new MethodResult<TValueClass>();
            methodResult._exception = ex;
            methodResult.IsOk = false;
            methodResult.Message = message;
            methodResult.ResultLevel = level;
            methodResult.ExceptionMessage = methodResult.GetFullExceptionMessage();
            return methodResult;
        }

        public static MethodResult<TValueClass> GetSuccessResult(TValueClass value, string message = "")
        {
            return new MethodResult<TValueClass>()
            {
                _exception = (Exception)null,
                IsOk = true,
                Message = message,
                ResultLevel = MethodResultLevel.OK,
                Value = value
            };
        }
    }
}
