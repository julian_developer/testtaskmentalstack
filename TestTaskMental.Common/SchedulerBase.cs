﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace TestTaskMental.Common
{
    /// <summary>Базовая реализация таймера</summary>
    public abstract class SchedulerBase
    {
        private ManualResetEvent _resetEvent = new ManualResetEvent(false);
        private Timer _timer;
        private Thread _tikThread;

        /// <summary>
        /// Возвращает время в миллисекундах, через которое будет срабатывать таймер.
        /// По умолчанию 5 минут.
        /// </summary>
        protected virtual int Interval
        {
            get => 300000;
            private set { }
        }

        /// <summary>
        /// Возвращает максимальное время выполнения итерации таймера.
        /// </summary>
        protected virtual TimeSpan MaxRumTime
        {
            get
            {
                return TimeSpan.FromMinutes(20.0);
            }
        }

        /// <summary>
        /// Указывает, что нужно ограничить время выполнения итерации таймера.
        /// </summary>
        protected virtual bool IsCheckMaxTime
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Возвращает вмемя (в миллисесундах) через которое произойдет первое срабатывание таймера.
        /// </summary>
        protected virtual int StartTimeout
        {
            get
            {
                return Interval;
            }
        }

        /// <summary>Запускает таймер.</summary>
        public virtual void Start()
        {
            if (_timer != null)
            {
                return;
            }

            _timer = new Timer(new TimerCallback(TimerProcess), (object)null, StartTimeout, -1);
        }

        public bool IsRun
        {
            get
            {
                return _timer != null;
            }
        }

        /// <summary>Останавливает таймер.</summary>
        public virtual void Stop()
        {
            if (_timer == null)
            {
                return;
            }

            _timer.Change(-1, -1);
            _timer = (Timer)null;
        }

        private void TurnOnTimer()
        {
            if (_timer == null)
            {
                return;
            }

            _timer.Change(Interval, -1);
        }

        /// <summary>Метод, вызываемый при срабатывании таймера.</summary>
        protected abstract void TimerTick();

        private void TimerProcess(object state)
        {
            try
            {
                _resetEvent.Reset();
                if (IsCheckMaxTime)
                {
                    if (!ThreadPool.QueueUserWorkItem(new WaitCallback(RunInternall)))
                    {
                        throw new Exception("Не удалось добавить выполнение итерации таймера в пулл потоков");
                    }

                    if (!_resetEvent.WaitOne(MaxRumTime))
                    {
                        if (_tikThread != null)
                        {
                            _tikThread.Abort();
                        }
                    }
                }
                else
                    RunInternall((object)null);
            }
            catch (Exception ex)
            {
                ProcessError(ex);
            }

            TurnOnTimer();
        }

        private void RunInternall(object state)
        {
            try
            {
                _tikThread = Thread.CurrentThread;
                TimerTick();
            }
            catch (Exception ex)
            {
                ProcessError(ex);
            }
            finally
            {
                _resetEvent.Set();
            }
        }

        /// <summary>
        /// Обработка исключения таймера вызванное в <see cref="M:ShedulerBase.TimerTick" />.
        /// </summary>
        /// <param name="exception"></param>
        protected abstract void ProcessError(Exception exception);
    }
}
