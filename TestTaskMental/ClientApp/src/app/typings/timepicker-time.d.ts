export interface TimepickerTime {
  hour: number;
  minute: number;
}
