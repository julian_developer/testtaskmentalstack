import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { Schedule } from '../../models/schedule.model';

@Component({
  selector: 'app-websocket-notification',
  templateUrl: './websocket-notification.component.html',
  styleUrls: ['./websocket-notification.component.scss']
})
export class WebSocketNotificationComponent implements OnInit {
  private _webSocketNotificationSchedule: Schedule;
  private _webSocketNotificationShow: boolean;

  @Input()
  set webSocketNotificationSchedule(webSocketNotificationSchedule: Schedule) {
    this._webSocketNotificationSchedule = webSocketNotificationSchedule;
  }
  get webSocketNotificationSchedule(): Schedule {
    return this._webSocketNotificationSchedule;
  }

  @Input()
  set webSocketNotificationShow(webSocketNotificationShow: boolean) {
    this._webSocketNotificationShow = webSocketNotificationShow;
  }
  get webSocketNotificationShow(): boolean {
    return this._webSocketNotificationShow;
  }
  @Output() onChangeSchedule: EventEmitter<Schedule> = new EventEmitter();
  @Output() onEndSchedule: EventEmitter<number> = new EventEmitter();

  private notificationArray: string[] = [];

  ngOnInit(): void {
    this.notificationArray = this.webSocketNotificationSchedule.notificationArray;
  }
  constructor() {}

  private onChangeScheduleNotification(item: string): void {
    this.webSocketNotificationSchedule.notificationTime = item;
    this.onChangeSchedule.emit(this.webSocketNotificationSchedule);
  }

  private doneSchedule(): void {
    this.onEndSchedule.emit(this.webSocketNotificationSchedule.id);
  }
}
