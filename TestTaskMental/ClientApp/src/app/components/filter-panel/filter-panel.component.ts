import { Component, Input, Output, OnInit, EventEmitter } from "@angular/core";
import { ScheduleType } from "../../models/schedule-type.model";
import { NgbPanelChangeEvent } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-filter-panel",
  templateUrl: "./filter-panel.component.html",
  styleUrls: ["./filter-panel.component.scss"]
})
export class FilterPanelComponent implements OnInit {
  @Input() filterScheduleTypes: ScheduleType[];
  @Output() onChangeFilter: EventEmitter<ScheduleType> = new EventEmitter();

  private selectedPriority: ScheduleType;
  private isExpanded: boolean = true;
  ngOnInit(): void {
    this.selectedPriority = this.filterScheduleTypes[0];
  }
  constructor() {}

  private onChangeFilterPriority(scheduleType: ScheduleType): void {
    this.filterScheduleTypes.forEach(x => (x.filterIsActive = false));
    scheduleType.filterIsActive = true;
    this.selectedPriority = scheduleType;
    this.onChangeFilter.emit(this.selectedPriority);
  }

  private changeExpandedFilterPanel(event: NgbPanelChangeEvent): void {
    this.isExpanded = event.nextState;
  }
}
