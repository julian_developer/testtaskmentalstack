import { Component, ViewChild } from "@angular/core";
import { ElementRef } from "@angular/core";
import { HostListener } from "@angular/core";
import { OnInit } from "@angular/core";
import { HttpProxyService } from "../../services/http-proxy.service";
import { ScheduleType } from '../../models/schedule-type.model';
import { Schedule } from '../../models/schedule.model';
import { from } from 'rxjs';
import { groupBy, mergeMap, toArray, debounceTime } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { NgbPanelChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { WebsocketService } from '../../services/websocket.service';

@Component({
  selector: 'app-schedule-list',
  templateUrl: './schedule-list.component.html',
  styleUrls: ['./schedule-list.component.scss']
})
export class ScheduleListComponent implements OnInit {
  private notificationArray: string[] = [];
  private scheduleTypes: ScheduleType[] = [];
  private filterScheduleTypes: ScheduleType[] = [];
  private schedules: Schedule[] = [];
  private schedulesDataSource: Schedule[] = [];
  private groupSchedules: Schedule[] = [];
  private newSchedule: Schedule = new Schedule();
  private notificationMessage: string;
  private notificationType: string;
  private notifier = new Subject<string>();
  private webSocketNotificationShow: boolean;
  private webSocketNotificationSchedule: Schedule = new Schedule();

  ngOnInit(): void {
    this.notificationArray = this.newSchedule.notificationArray;
    this.loadScheduleTypes();
    this.configNotifier();

    const subject = this.websocketService.connect('ws://' + window.location.host + '/notification');
    subject.subscribe(
      response => {
        const schedule = Schedule.mapFromJsonObject(response.data);
        const priority = this.scheduleTypes.find(x => x.id === schedule.type);
        schedule.priority = priority;
        this.webSocketNotificationSchedule = schedule;
        this.webSocketNotificationShow = true;
      },
      error => {
        console.error(error);
        this.notifierError(
          'Не удалось подключиться к веб сокету. Попробуйте еще раз или обратитесь в службу технической поддержки'
        );
      }
    );
  }

  constructor(
    private httpProxyService: HttpProxyService,
    private modalService: NgbModal,
    private websocketService: WebsocketService
  ) {}

  private configNotifier() {
    this.notifier.subscribe(message => (this.notificationMessage = message));
    this.notifier.pipe(debounceTime(5000)).subscribe(() => (this.notificationMessage = null));
  }

  private notifierSuccess(message: string): void {
    this.notificationType = 'success';
    this.notifier.next(message);
  }

  private notifierError(message: string): void {
    this.notificationType = 'error';
    this.notifier.next(message);
  }

  private loadScheduleTypes(): void {
    this.httpProxyService.getScheduleTypes().subscribe(
      response => {
        this.scheduleTypes = response;
        this.filterScheduleTypes = [...response];
        const filterAll = new ScheduleType(0, 'All');
        filterAll.filterIsActive = true;
        this.filterScheduleTypes.unshift(filterAll);
        this.loadSchedules();
      },
      error => {
        console.error(error);
        this.notifierError(
          'Не удалось загрузить данные. Попробуйте еще раз или обратитесь в службу технической поддержки'
        );
      }
    );
  }

  private onScheduleTimeChange(isOpen: boolean, schedule: Schedule) {
    if (!isOpen && schedule !== null) {
      const scheduleTime = moment(schedule.scheduleDateTime).toDate();
      scheduleTime.setHours(schedule.scheduleTime.hour);
      scheduleTime.setMinutes(schedule.scheduleTime.minute);
      scheduleTime.setSeconds(0);
      scheduleTime.setMilliseconds(0);

      schedule.scheduleDateTime = scheduleTime;
      if (schedule.notificationTime) {
        this.fillNotificationTime(schedule, schedule.notificationTime);
      }

      this.updateSchedule(schedule);
    } else if (!isOpen && schedule === null) {
      (this.newSchedule.scheduleDateTime as any).month = (this.newSchedule.scheduleDateTime as any).month - 1;
      const scheduleTime = moment(this.newSchedule.scheduleDateTime).toDate();
      scheduleTime.setHours(this.newSchedule.scheduleTime.hour);
      scheduleTime.setMinutes(this.newSchedule.scheduleTime.minute);
      scheduleTime.setSeconds(0);
      scheduleTime.setMilliseconds(0);
      if (this.newSchedule.notificationTime) {
        this.fillNotificationTime(this.newSchedule, this.newSchedule.notificationTime);
      }

      this.newSchedule.scheduleDateTime = scheduleTime;
    }
  }

  private onChangeNotification(schedule: Schedule, notification: string): void {
    if (schedule !== null) {
      schedule.notificationTime = notification;
      this.fillNotificationTime(schedule, notification);
      this.updateSchedule(schedule);
      return;
    }

    this.newSchedule.notificationTime = notification;
    this.fillNotificationTime(this.newSchedule, notification);
  }

  private fillNotificationTime(schedule: Schedule, notification: string): void {
    let notificationDate = new Date();
    switch (notification) {
      case '5 min.':
        notificationDate = moment(schedule.scheduleDateTime)
          .add(-5, 'minutes')
          .toDate();
        break;
      case '10 min.':
        notificationDate = moment(schedule.scheduleDateTime)
          .add(-10, 'minutes')
          .toDate();
        break;
      case '30 min.':
        notificationDate = moment(schedule.scheduleDateTime)
          .add(-30, 'minutes')
          .toDate();
        break;
      case '1 hour':
        notificationDate = moment(schedule.scheduleDateTime)
          .add(-1, 'hours')
          .toDate();
        break;
      case '3 hour':
        notificationDate = moment(schedule.scheduleDateTime)
          .add(-3, 'hours')
          .toDate();
        break;
      case '1 day':
        notificationDate = moment(schedule.scheduleDateTime)
          .add(-1, 'days')
          .toDate();
        break;
      case '1 week':
        notificationDate = moment(schedule.scheduleDateTime)
          .add(-7, 'days')
          .toDate();
        break;
    }
    notificationDate.setSeconds(0);
    notificationDate.setMilliseconds(0);
    schedule.notificationDateTime = notificationDate;
  }

  private onChangePriority(schedule: Schedule, scheduleType: ScheduleType): void {
    if (schedule !== null) {
      schedule.type = scheduleType.id;
      schedule.priority = scheduleType;
      this.updateSchedule(schedule);
      return;
    }

    this.newSchedule.type = scheduleType.id;
    this.newSchedule.priority = scheduleType;
  }

  private loadSchedules(): void {
    this.httpProxyService.getSchedules().subscribe(
      response => {
        this.schedules = response;
        this.schedulesDataSource = response;
        this.mapPriority();
      },
      error => {
        console.error(error);
        this.notifierError(
          'Не удалось загрузить данные. Попробуйте еще раз или обратитесь в службу технической поддержки'
        );
      }
    );
  }

  private mapPriority(): void {
    this.schedules.forEach(schedule => {
      const priority = this.scheduleTypes.find(x => x.id === schedule.type);
      schedule.priority = priority;
    });
  }

  private addSchedule(): void {
    this.httpProxyService.addSchedule(this.newSchedule).subscribe(
      response => {
        this.newSchedule = new Schedule();
        const newItem = Schedule.mapFromServerObject(response.value);
        this.schedulesDataSource.push(newItem);
        this.schedules = [...this.schedulesDataSource];
        this.mapPriority();
        this.notifierSuccess('Задача добавлена');
      },
      error => {
        console.error(error);
        this.notifierError(
          'Не удалось добавить задачу. Попробуйте еще раз или обратитесь в службу технической поддержки'
        );
      }
    );
  }

  private updateSchedule(schedule: Schedule): void {
    this.httpProxyService.updateSchedule(schedule).subscribe(
      response => {
        this.notifierSuccess('Задача обновлена');
        this.webSocketNotificationShow = false;
      },
      error => {
        console.error(error);
        this.notifierError(
          'Не удалось обновить задачу. Попробуйте еще раз или обратитесь в службу технической поддержки'
        );
      }
    );
  }

  private removeSchedule(scheduleId: number): void {
    this.httpProxyService.removeSchedule(scheduleId).subscribe(
      response => {
        this.schedules = this.schedulesDataSource.filter(x => x.id !== scheduleId);
        this.notifierSuccess('Задача удалена');
        this.webSocketNotificationShow = false;
      },
      error => {
        console.error(error);
        this.notifierError(
          'Не удалось удалить задачу. Попробуйте еще раз или обратитесь в службу технической поддержки'
        );
      }
    );
  }

  private openConfirmDeleteDialog(content: any, scheduleId: number): void {
    this.modalService.open(content).result.then(result => {
      if (result === 'Delete') {
        this.removeSchedule(scheduleId);
      }
    });
  }

  private onChangeFilter(scheduleType: ScheduleType): void {
    if (scheduleType.id === 0) {
      this.schedules = this.schedulesDataSource;
      return;
    }

    this.schedules = this.schedulesDataSource.filter(x => x.type === scheduleType.id);
  }

  private changeExpandedTask(event: NgbPanelChangeEvent, dataItem: any): void {
    dataItem.isExpanded = event.nextState;
  }

  private onEditDescription(event: any, dataItem: Schedule): void {
    dataItem.editDescription = !dataItem.editDescription;

    if (!dataItem.editDescription) {
      this.updateSchedule(dataItem);
    }
  }
}
