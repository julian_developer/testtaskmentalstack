export enum ScheduleTypeEnum {
  Urgently = 1,
  Important = 2,
  Normal = 3,
  Neutral = 4
}
