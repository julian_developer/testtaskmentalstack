import { TimepickerTime } from '../typings/timepicker-time';
import * as moment from 'moment';
import { ScheduleType } from './schedule-type.model';

export class Schedule {
  public id: number;
  public name: string;
  public description: string;
  public type: number;
  public scheduleDateTime: Date;
  public notificationDateTime: Date;
  public scheduleDate: string;
  public scheduleTime: TimepickerTime;
  public notificationTime: string;
  public editDescription: boolean;
  public priority: ScheduleType;
  public notificationArray: string[] = ['5 min.', '10 min.', '30 min.', '1 hour', '3 hours', '1 day', '1 week'];
  private days: string[] = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  private months: string[] = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];

  constructor(
    id: number = 0,
    name: string = '',
    description: string = '',
    type: number = 0,
    scheduleDateTime: Date = new Date(),
    notificationDateTime: Date = new Date()
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.type = type;
    this.scheduleDateTime = scheduleDateTime;
    this.editDescription = false;
    this.notificationDateTime = notificationDateTime;
    this.scheduleDate = this.getStringDate(scheduleDateTime);
    this.scheduleTime = this.getTime(scheduleDateTime);
    this.notificationTime = this.getNotificationTime(scheduleDateTime, notificationDateTime);
    this.priority = new ScheduleType();
  }

  public static mapFromServerObject(jsonObject: Schedule): Schedule {
    return new Schedule(
      jsonObject.id,
      jsonObject.name,
      jsonObject.description,
      jsonObject.type,
      jsonObject.scheduleDateTime,
      jsonObject.notificationDateTime
    );
  }

  public static mapFromJsonObject(jsonObject: string): Schedule {
    const model = JSON.parse(jsonObject);
    return new Schedule(
      model.Id,
      model.Name,
      model.Description,
      model.Type,
      model.ScheduleDateTime,
      model.NotificationDateTime
    );
  }

  private getNotificationTime(scheduleDateTime: Date, notificationDateTime: Date): string {
    const scheduleDate = moment(new Date(scheduleDateTime));
    const notificationDate = moment(new Date(notificationDateTime));

    const diff = scheduleDate.diff(notificationDate);
    const diffDuration = moment.duration(diff);
    if (diffDuration.minutes() > 0) {
      const minutes = diffDuration.minutes();
      return minutes === 5
        ? this.notificationArray[0]
        : minutes === 10
        ? this.notificationArray[1]
        : this.notificationArray[2];
    }

    if (diffDuration.hours() > 0) {
      const hours = diffDuration.hours();
      return hours === 1 ? this.notificationArray[3] : this.notificationArray[4];
    }

    if (diffDuration.days() > 0) {
      const days = diffDuration.days();
      return days === 1 ? this.notificationArray[5] : this.notificationArray[6];
    }

    return '';
  }

  private getTime(scheduleDateTime: Date): TimepickerTime {
    const result = new Date(scheduleDateTime);
    return { hour: result.getHours(), minute: result.getMinutes() };
  }

  private getStringDate(scheduleDateTime: Date): string {
    const result = new Date(scheduleDateTime);
    result.setHours(0);
    result.setMinutes(0);
    result.setSeconds(0);
    result.setMilliseconds(0);
    let day = this.days[result.getDay()];
    const now = new Date();
    if (now.getDay() === result.getDay()) {
      day = 'Today';
    }

    if (now.getDay() + 1 === result.getDay()) {
      day = 'Tomorrow';
    }

    const month = this.months[result.getMonth()];
    return `${day}, ${month} ${result.getDate()}`;
  }
}
