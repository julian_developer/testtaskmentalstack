import { ScheduleTypeEnum } from './schedule-type.enum';

export class ScheduleType {
  public id: number;
  public name: string;
  public className: string;
  public filterText: string;
  public filterIsActive: boolean;

  constructor(id: number = 0, name: string = '') {
    this.id = id;
    this.name = name;
    this.className = this.getClassNameByScheduleType(id);
    this.filterText = name.replace(' priority', '');
  }

  public static mapFromServerObject(jsonObject: ScheduleType): ScheduleType {
    return new ScheduleType(jsonObject.id, jsonObject.name);
  }

  private getClassNameByScheduleType(scheduleType: number): string {
    switch (scheduleType) {
      case ScheduleTypeEnum.Urgently:
        return 'urgently-priority-dot';
      case ScheduleTypeEnum.Important:
        return 'important-priority-dot';
      case ScheduleTypeEnum.Normal:
        return 'normal-priority-dot';
      case ScheduleTypeEnum.Neutral:
        return 'neutral-priority-dot';
      default:
        return 'neutral-priority-dot';
    }
  }
}
