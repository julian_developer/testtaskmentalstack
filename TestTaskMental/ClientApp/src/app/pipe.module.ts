import { NgModule } from '@angular/core';
import { GroupByPipe } from './pipes/group-by.pipe';

@NgModule({
  imports: [],
  declarations: [GroupByPipe],
  exports: [GroupByPipe]
})
export class PipeModule {
  static forRoot() {
    return {
      ngModule: PipeModule,
      providers: []
    };
  }
}
