import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Schedule } from '../models/schedule.model';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ScheduleType } from '../models/schedule-type.model';
import { HttpHeaders } from '@angular/common/http';
import * as moment from 'moment';

@Injectable()
export class HttpProxyService {
  constructor(public http: HttpClient) {}

  public getSchedules(): Observable<Schedule[]> {
    return this.http
      .get<Schedule[]>('api/Schedule/GetAllSchedules')
      .pipe(map(response => response.map(item => Schedule.mapFromServerObject(item))));
  }

  public getScheduleTypes(): Observable<ScheduleType[]> {
    return this.http
      .get<ScheduleType[]>('api/Schedule/GetAllScheduleTypes')
      .pipe(map(response => response.map(item => ScheduleType.mapFromServerObject(item))));
  }

  public addSchedule(schedule: Schedule): Observable<any> {
    const body = JSON.stringify(schedule, (key, value) => {
      if (key === 'scheduleDateTime') {
        return moment(value).format();
      }

      if (key === 'notificationDateTime') {
        return moment(value).format();
      }

      return value;
    });
    const headerOptions = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<Schedule>('api/Schedule/AddSchedule', body, { headers: headerOptions });
  }

  public updateSchedule(schedule: Schedule): Observable<Schedule> {
    const body = JSON.stringify(schedule, (key, value) => {
      if (key === 'scheduleDateTime') {
        return moment(value).format();
      }

      if (key === 'notificationDateTime') {
        return moment(value).format();
      }

      return value;
    });
    const headerOptions = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<Schedule>('api/Schedule/UpdateSchedule', body, { headers: headerOptions });
  }

  public removeSchedule(scheduleId: number): Observable<Schedule> {
    const params = new HttpParams().set('scheduleId', scheduleId.toString());
    return this.http.get<Schedule>('api/Schedule/RemoveSchedule', { params });
  }
}
