import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { ScheduleListComponent } from './components/schedule-list/schedule-list.component';
import { HttpProxyService } from './services/http-proxy.service';
import { PipeModule } from './pipe.module';
import { FilterPanelComponent } from './components/filter-panel/filter-panel.component';
import { WebsocketService } from './services/websocket.service';
import { APP_BASE_HREF } from '@angular/common';
import { WebSocketNotificationComponent } from './components/websocket-notification/websocket-notification.component';

@NgModule({
  declarations: [AppComponent, ScheduleListComponent, FilterPanelComponent, WebSocketNotificationComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    NgbModule,
    RouterModule.forRoot([{ path: '', component: ScheduleListComponent, pathMatch: 'full' }]),
    PipeModule.forRoot()
  ],
  providers: [HttpProxyService, WebsocketService, { provide: APP_BASE_HREF, useValue: '/' }],
  bootstrap: [AppComponent]
})
export class AppModule {}
