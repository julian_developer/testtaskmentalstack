using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using TestTaskMental.BL.Engine;
using TestTaskMental.BL.Mapper;
using TestTaskMental.DAL;
using TestTaskMental.Logic.NotificationSender;
using TestTaskMental.Logic.WebSocketHandlers;
using TestTaskMental.WebSocketEngine;

namespace TestTaskMental
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddWebSocketManager();
            services.AddAutofac();

            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

            string connection = Configuration.GetConnectionString("mentalDb");

            services.AddDbContext<Context>(options =>
                options.UseSqlServer(connection));

            var builder = new ContainerBuilder();

            builder.Populate(services);
            builder.RegisterType<ScheduleEngine>().As<IScheduleEngine>();
            builder.RegisterType<ScheduleMapper>().As<IScheduleMapper>();
            builder.RegisterType<Context>().As<IContext>();
            builder.RegisterType<NotificationSender>().As<INotificationSender>();
            var applicationContainer = builder.Build();

            return new AutofacServiceProvider(applicationContainer);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseWebSockets();

            app.MapWebSocketManager("/notification", serviceProvider.GetService<NotificationWebSocketHandler>());
        }
    }
}
