using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TestTaskMental.BL.Engine;
using TestTaskMental.BL.Models;
using TestTaskMental.Common;
using TestTaskMental.Logic.NotificationSender;

namespace TestTaskMental.Controllers
{
    [Route("api/[controller]")]
    public class ScheduleController : Controller
    {
        private readonly IScheduleEngine _engine;
        private readonly ILogger<ScheduleController> _logger;
        private readonly INotificationSender _notificationSender;
        private static string UserLogin = "DefaultUser";

        public ScheduleController(IScheduleEngine engine, ILogger<ScheduleController> logger, INotificationSender notificationSender)
        {
            _engine = engine;
            _logger = logger;
            _notificationSender = notificationSender;
        }

        [HttpGet("[action]")]
        public List<ScheduleDto> GetAllSchedules()
        {
            try
            {
                var result = _engine.GetAllSchedules(UserLogin);
                if (result.IsOk)
                {
                    return result.Value;
                }

                return new List<ScheduleDto>();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetAllSchedules error");
                return new List<ScheduleDto>();
            }
        }

        [HttpGet("[action]")]
        public List<ScheduleTypeDto> GetAllScheduleTypes()
        {
            try
            {
                var result = _engine.GetAllScheduleTypes();
                if (result.IsOk)
                {
                    return result.Value;
                }

                return new List<ScheduleTypeDto>();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetAllScheduleTypes error");
                return new List<ScheduleTypeDto>();
            }
        }

        [HttpGet("[action]")]
        public JsonResult RemoveSchedule(long scheduleId)
        {
            try
            {
                var result = _engine.RemoveSchedule(scheduleId, UserLogin);
                return Json(result);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "RemoveSchedule error");
                return Json(e.ToErrorMethodResult<ScheduleDto>());
            }
        }

        [HttpPost("[action]")]
        public JsonResult AddSchedule([FromBody] ScheduleDto schedule)
        {
            try
            {
                var result = _engine.AddSchedule(schedule, UserLogin);
                return Json(result);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "AddSchedule error");
                return Json(e.ToErrorMethodResult<ScheduleDto>());
            }
        }

        [HttpPost("[action]")]
        public JsonResult UpdateSchedule([FromBody] ScheduleDto schedule)
        {
            try
            {
                var result = _engine.UpdateSchedule(schedule, UserLogin);
                return Json(result);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "UpdateSchedule error");
                return Json(e.ToErrorMethodResult<ScheduleDto>());
            }
        }
    }
}
