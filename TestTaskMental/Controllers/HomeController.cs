﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestTaskMental.Logic.NotificationSender;

namespace TestTaskMental.Controllers
{
    public class HomeController : Controller
    {
        public HomeController(INotificationSender notificationSender)
        {
            notificationSender.Start();
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}