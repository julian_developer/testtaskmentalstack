﻿using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestTaskMental.WebSocketEngine
{
    public class WebSocketHandler
    {
        private WebSocketConnectionManager _webSocketConnectionManager;
        private string socketId;

        public WebSocketHandler(WebSocketConnectionManager webSocketConnectionManager)
        {
            _webSocketConnectionManager = webSocketConnectionManager;
        }

        public void OnConnected(WebSocket socket)
        {
            socketId = _webSocketConnectionManager.AddSocket(socket);
        }

        public async Task OnDisconnected(WebSocket socket)
        {
            var socketId = _webSocketConnectionManager.GetSocketId(socket);
            await _webSocketConnectionManager.RemoveSocket(socketId);
        }

        public async Task SendNotificationToAllAsync(string notification)
        {
            var sockets = _webSocketConnectionManager.GetAllSockets();
            foreach (var socket in sockets.Values)
            {
                if (socket == null || socket.State != WebSocketState.Open)
                {
                    return;
                }

                var buffer = new ArraySegment<byte>(Encoding.ASCII.GetBytes(notification), 0, notification.Length);

                await socket.SendAsync(buffer, WebSocketMessageType.Text, true, CancellationToken.None);
            }
            
        }
    }
}
