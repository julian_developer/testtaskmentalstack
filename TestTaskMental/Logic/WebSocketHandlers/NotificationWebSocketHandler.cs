﻿using TestTaskMental.WebSocketEngine;

namespace TestTaskMental.Logic.WebSocketHandlers
{
    public class NotificationWebSocketHandler: WebSocketHandler
    {
        public NotificationWebSocketHandler(WebSocketConnectionManager webSocketConnectionManager) : base(webSocketConnectionManager)
        {
        }
    }
}
