﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTaskMental.Logic.NotificationSender
{
    public interface INotificationSender
    {
        void Start();
    }
}
