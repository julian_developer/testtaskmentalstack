﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using TestTaskMental.BL.Engine;
using TestTaskMental.Common;
using TestTaskMental.Logic.WebSocketHandlers;
using Microsoft.Extensions.Configuration;

namespace TestTaskMental.Logic.NotificationSender
{
    public class NotificationSender: INotificationSender
    {
        private readonly ILogger<NotificationSender> _logger;
        private readonly NotificationWebSocketHandler _realTimeWebSocketHandler;
        private readonly IScheduleEngine _engine;
        private readonly IConfiguration _configuration;
        private Timer _timer;
        private static string UserLogin = "DefaultUser";

        public NotificationSender(ILogger<NotificationSender> logger, IScheduleEngine engine, IConfiguration configuration, NotificationWebSocketHandler realTimeWebSocketHandler)
        {
            _logger = logger;
            _engine = engine;
            _configuration = configuration;
            _realTimeWebSocketHandler = realTimeWebSocketHandler;
        }

        public void Start()
        {
            _timer = new Timer(async callback => await SendNotification(), null, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(30));
        }

        public async Task SendNotification()
        {
            var connectionString = _configuration["ConnectionStrings:mentalDb"];
            var notificationsResult = _engine.GetAllSchedulesForNotification(UserLogin, connectionString);
            if (!notificationsResult.IsOk)
            {
                return;
            }

            var notifications = notificationsResult.Value;

            foreach (var notification in notifications)
            {
                await _realTimeWebSocketHandler.SendNotificationToAllAsync(JsonConvert.SerializeObject(notification));
            }
        }
    }
}
